{******************************************************************************}
{ Tabela CEST                                                                  }
{                                                                              }
{ Funcionalidades:                                                             }
{ Efetua a leitura do site da lei do conv�nio onde � criado a CEST e retorna os}
{ c�digos da CEST x NCM e a descri��o                                          }
{ https://www.confaz.fazenda.gov.br/legislacao/convenios/2015/cv146_15         }
{                                                                              }
{ Direitos Autorais Reservados (c) 2017 J�ter Rabelo Ferreira                  }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{                                                                              }
{ J�ter Rabelo Ferreira  -  jeter.rabelo@jerasoft.com.br                       }
{ Jera Soft Co.� - Campestre/MG                                                }
{                                                                              }
{ Desenvolvedor desta unit:                                                    }
{   Lucas da Silva Ferreira -  lucas.ferreira@jerasoft.com.br                  }
{                                                                              }
{******************************************************************************}

unit Jera.LeituraCEST;

interface

uses
  SysUtils, Generics.Collections, Forms, Dialogs, StrUtils, System.Classes, System.Math;

type
  TTabelaCEST = class
  private
    FDescricao: string;
    FCEST: string;
    FNCM: string;
  public
    property CEST: string read FCEST write FCEST;
    property NCM: string read FNCM write FNCM;
    property Descricao: string read FDescricao write FDescricao;
  end;

  TTabelaCESTItems = class(TObjectList<TTabelaCEST>)
  end;

  TLeituraCEST = class
  private
    FItens: TTabelaCESTItems;
    FURL: string;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Ler;
    property URL: string read FURL write FURL;
    property Itens: TTabelaCESTItems read FItens;
  end;

function RetornarConteudo(var AText: string; const AIni: string; const AFim: string; ARemoveFim:
  Boolean = True; const ATerm: string = ''): string;

implementation

uses
  IdHTTP, System.Types;

function _RetornarConteudo(var AText: string; const AIni: string; const AFim: string; ARemoveFim:
  Boolean; const ATerm: string = ''): string;
var
  I: Integer;
  S: string;
  VPos: Integer;
begin
  Result := '';
  I := Pos(AIni, AText);
  if (I = 0) and (Length(AIni) > 0) then
    Exit;
  S := Copy(AText, Max(I, 1) + Length(AIni), MaxInt);
  VPos := Pos(AFim, S);
  Result := Copy(S, 1, VPos - 1);
  if (Result <> '') then
  begin
    AText := StringReplace(AText, AIni + Result + IfThen(ARemoveFim, AFim, ''), '', []);
    if (AText = ATerm) then
      AText := '';
  end;
end;

function RetornarConteudo(var AText: string; const AIni: string; const AFim: string; ARemoveFim:
  Boolean = True; const ATerm: string = ''): string;
var
  VFim: string;
  VRemoveFim: Boolean;
begin
  VFim := ATerm;
  VRemoveFim := (Pos(AFim, AText) > 0);
  if VRemoveFim then
    VFim := AFim;
  Result := _RetornarConteudo(AText, AIni, VFim, VRemoveFim);
end;

{ TLeituraCEST }

constructor TLeituraCEST.Create;
begin
  inherited;
  FItens := TTabelaCESTItems.Create;
  URL := 'https://www.confaz.fazenda.gov.br/legislacao/convenios/2017/CV052_17';
end;

destructor TLeituraCEST.Destroy;
begin
  FreeAndNil(FItens);
  inherited;
end;

procedure TLeituraCEST.Ler;
var
  FLista: TList<string>;
const
  tagInicio = '<table class="plain">';
  tagFim = '</table>';
  trInicio = '<tr>';
  trFim = '</tr>';
  tdInicio = '<td>';
  tdFim = '</td>';
  pInicio = '>';
  pFim = '</p>';

  procedure _AddLista(ATabela: string);
  var
    AQtde: Integer;
    ADeslocamento: Integer;
    AInicio1: Integer;
    AFim1: Integer;
    ALinha: string;
  begin
    ADeslocamento := 1;
    while (Pos(trInicio, ATabela, ADeslocamento) > 0) do
    begin
      AInicio1 := Pos(trInicio, ATabela, ADeslocamento);
      AFim1 := Pos(trFim, ATabela, AInicio1);
      AQtde := (AFim1 - AInicio1) + trFim.Length;
      ADeslocamento := AFim1;
      ALinha := Copy(ATabela, AInicio1, AQtde);
      if ALinha.contains('<td colspan="') then
        Continue;
      ALinha := StringReplace(ALinha, '<tr>', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '</tr>', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '</p>', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<td>', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<p class="TabelaSubtitulo">', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<p align="center" class="Tabelaesquerda">', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<p class="Tabelajustificado">', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<p class="', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<td colspan="2">', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, 'TabelaSubtituloverde', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, 'Tabelajustificadoverde', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '&#174', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '">', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '</td>', '|', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '<br>', '', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, #13, ' ', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, #10, ' ', [rfReplaceAll]);
      ALinha := StringReplace(ALinha, '  ', ' ', [rfReplaceAll]);
      ALinha := TrimLeft(ALinha.Trim);
      if FLista.IndexOf(ALinha) = -1 then
        FLista.Add(ALinha);
    end;
  end;

  procedure _CriaOBJs;

    procedure _CarregaLista(const ALinha: string);

      procedure _AddLista(const ACest: string; const ANcm: string; const ADescricao: string);
      var
        VObj: TTabelaCEST;
        VCest: string;
        VNcm: string;
        VDescricao: string;

        function FindCEST(const ACest: string): Boolean;
        var
          VObj: TTabelaCEST;
        begin
          for VObj in FItens do
            if SameText(VObj.CEST, ACest) then
              Exit(True);
          Result := False;
        end;

      begin
        VCest := TrimLeft(ACest).Trim;
        VNcm := TrimLeft(ANcm).Trim;
        VDescricao := TrimLeft(ADescricao).Trim;
        if (VCest = '') or VCest.contains('CEST') or VCest.StartsWith('28.0') or VCest.StartsWith('28.999') or
        VCest.Contains('Raz�o Social') then
          Exit;

        VObj := TTabelaCEST.Create;
        VObj.CEST := VCest;
        VObj.NCM := VNcm;
        VObj.Descricao := VDescricao;
        if (VCest.Trim <> '') and (not FindCEST(VCest)) then
        FItens.Add(VObj);
      end;

    var
      SLinhaNCM: TArray<string>;
    begin
      SLinhaNCM := ALinha.Split(['|']);
      if Length(SLinhaNCM) > 2 then
        _AddLista(SLinhaNCM[1], SLinhaNCM[2], SLinhaNCM[3]);
    end;

  var
    I: Integer;
  begin
    for I := 0 to Pred(FLista.Count) do
      _CarregaLista(FLista[I]);
  end;

var
  FConteudo: string;
  VHttp: TIdHTTP;
  SSite: TArray<string>;
  I: Integer;
  VTabela: string;
  VTabelas: string;
begin
  VHttp := TIdHTTP.Create(nil);
  FLista := TList<string>.Create;
  try
    FConteudo := VHttp.Get(URL);
    VTabelas := '';
    I := 0;
    repeat
      begin
        Inc(I);
        VTabela := RetornarConteudo(FConteudo, '<table class="plain">', '</table>', False, '');
        if I = 1 then
          Continue;
        VTabelas := Concat(VTabelas, VTabela, '�');
      end;
    until VTabela = '';
    SSite := VTabelas.Split(['�']);
    for I := 0 to High(SSite) do
      _AddLista(SSite[I]);
    _CriaOBJs;
  finally
    FreeAndNil(FLista);
    FreeAndNil(VHttp);
  end;
end;

end.

