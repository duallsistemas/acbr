unit uFrmMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    EdtURLLei: TLabeledEdit;
    CbResultado: TComboBox;
    Label1: TLabel;
    Panel1: TPanel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  Jera.LeituraCEST, Rest.Json;

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  FLeituraCEST: TLeituraCEST;
  VObj : TTabelaCEST;
  STemp: string;
const
  InsertInto = 'INSERT INTO cst_cest_ncm (codcest, ncm, descrcest, flagativo, dtalteracao) VALUES (%s, %s, %s, ''S'', current_timestamp);';
  Csv = '"%s"; "%s"; "%s"';
begin
  FLeituraCEST := TLeituraCEST.Create;
  try
    Memo1.Lines.Clear;
    FLeituraCEST.URL := EdtURLLei.Text;
    FLeituraCEST.Ler;
    case CbResultado.ItemIndex of
      0:
        begin
          Memo1.WordWrap := False;
          Memo1.Lines.BeginUpdate;
          for VObj in FLeituraCEST.Itens do
          begin
            if VObj.CEST.Trim='' then
            Continue;
            STemp := Format(Csv, [VObj.CEST,
                                  VObj.NCM,
                                  VObj.Descricao]);
            if STemp='" "; " "; " "' then
             begin
              STemp := '';
             end;
            if STemp <> '' then
            Memo1.Lines.Add(STemp);
          end;
        end;
      1:
        begin
          Memo1.WordWrap := True;
          Memo1.Lines.BeginUpdate;
          STemp := TJson.ObjectToJsonString(FLeituraCEST.Itens);
          STemp := StringReplace(STemp, ',null', '', [rfReplaceAll]);
          Memo1.Lines.Add(STemp);
        end;
      2:
        begin
          Memo1.WordWrap := False;
          Memo1.Lines.BeginUpdate;
          for VObj in FLeituraCEST.Itens do
          begin
            STemp := Format(InsertInto, [VObj.CEST.QuotedString,
                                         VObj.NCM.QuotedString,
                                         VObj.Descricao.QuotedString]);
            Memo1.Lines.Add(STemp);
          end;
        end;
    end;
  finally
    Memo1.Lines.EndUpdate;
    FreeAndNil(FLeituraCEST);
  end;
end;

end.
