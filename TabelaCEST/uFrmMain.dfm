object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Consultar Tabela CEST'
  ClientHeight = 512
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    767
    512)
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = 664
    Top = 3
    Width = 78
    Height = 16
    Anchors = [akTop, akRight]
    Caption = 'Resultado em'
  end
  object Button1: TButton
    Left = 8
    Top = 7
    Width = 75
    Height = 33
    Caption = 'Consultar'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 0
    Top = 44
    Width = 767
    Height = 464
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object EdtURLLei: TLabeledEdit
    Left = 89
    Top = 19
    Width = 569
    Height = 24
    Anchors = [akLeft, akTop, akRight]
    EditLabel.Width = 42
    EditLabel.Height = 16
    EditLabel.Caption = 'URL Lei'
    TabOrder = 2
    Text = 
      'https://www.confaz.fazenda.gov.br/legislacao/convenios/2017/CV05' +
      '2_17'
  end
  object CbResultado: TComboBox
    Left = 664
    Top = 19
    Width = 95
    Height = 24
    Style = csDropDownList
    Anchors = [akTop, akRight]
    ItemIndex = 0
    TabOrder = 3
    Text = 'CSV'
    Items.Strings = (
      'CSV'
      'Json'
      'Insert Into')
  end
  object Panel1: TPanel
    Left = 0
    Top = 480
    Width = 767
    Height = 32
    Align = alBottom
    Caption = #169' Copyright by Jera Soft Co. '#174' - 2017 - (Freeware)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
end
