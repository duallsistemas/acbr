unit pcnNFEUtilTest;

{$I ACBr.inc}

interface

uses
  Classes, SysUtils,
  {$ifdef FPC}
  fpcunit, testutils, testregistry, LConvEncoding
  {$else}
    TestFramework
  {$endif};

type

  { pcnGerarXML }

  pcnGerarXML = class(TTestCase)
  published
   procedure GerarXML;
  end;

implementation

  uses pcnNFe, pcnNFeW, pcnConversao;


{ pcnGerarXML }

procedure PreecherNFE(ANFe : TNFe);
var
  Produto: TDetCollectionItem;
  Volume: TVolCollectionItem;
  Duplicata: TDupCollectionItem;
  ObsComplementar: TobsContCollectionItem;
  ObsFisco: TobsFiscoCollectionItem;
begin
  ANFe.infNFe.Versao := 3.10;
  ANFe.Ide.cNF := 10001;
  ANFe.Emit.CNPJCPF           := '000000000000001';
  ANFe.Emit.IE                := '0000000000001';
  ANFe.Emit.xNome             := 'EMPRESA DE TESTE';
  ANFe.Emit.xFant             := 'EMPRESA DE TESTE';
  ANFe.Emit.EnderEmit.fone    := '000000000001';
  ANFe.Emit.EnderEmit.CEP     := 99999999;
  ANFe.Emit.EnderEmit.xLgr    := 'RUA DE TESTE';
  ANFe.Emit.EnderEmit.nro     := 'S/N';
  ANFe.Emit.EnderEmit.xCpl    := '';
  ANFe.Emit.EnderEmit.xBairro := 'ZONA RURAL';
  ANFe.Emit.EnderEmit.cMun    := 00000001;
  ANFe.Emit.EnderEmit.xMun    := 'NEW YORK';
  ANFe.Emit.EnderEmit.UF      := 'MA';
  ANFe.Emit.enderEmit.cPais   := 1058;
  ANFe.Emit.enderEmit.xPais   := 'BRASIL';

  ANFe.Emit.IEST              := '';
  ANFe.Emit.IM                := '2648800';
  ANFe.Emit.CNAE              := '6201500';
  ANFe.Emit.CRT               :=crtRegimeNormal;

  ANFe.Dest.CNPJCPF           := '05481336000137';
  ANFe.Dest.IE                := '687138770110';
  ANFe.Dest.ISUF              := '';
  ANFe.Dest.xNome             := 'D.J. COM. E LOCA��O DE SOFTWARES LTDA - ME';

  ANFe.Dest.EnderDest.Fone    := '1532599600';
  ANFe.Dest.EnderDest.CEP     := 18270170;
  ANFe.Dest.EnderDest.xLgr    := 'Rua Coronel Aureliano de Camargo';
  ANFe.Dest.EnderDest.nro     := '973';
  ANFe.Dest.EnderDest.xCpl    := '';
  ANFe.Dest.EnderDest.xBairro := 'Centro';
  ANFe.Dest.EnderDest.cMun    := 3554003;
  ANFe.Dest.EnderDest.xMun    := 'Tatui';
  ANFe.Dest.EnderDest.UF      := 'SP';
  ANFe.Dest.EnderDest.cPais   := 1058;
  ANFe.Dest.EnderDest.xPais   := 'BRASIL';

  Produto := ANFe.Det.Add;
  Produto.Prod.nItem    := 1;
  Produto.Prod.cProd    := '123456';
  Produto.Prod.cEAN     := '7896523206646';
  Produto.Prod.xProd    := 'TESTE DE PRODUTO';
  Produto.Prod.NCM      := '94051010';
  Produto.Prod.EXTIPI   := '';
  Produto.Prod.CFOP     := '5101';
  Produto.Prod.uCom     := 'UN';
  Produto.Prod.qCom     := 1 ;
  Produto.Prod.vUnCom   := 100;
  Produto.Prod.vProd    := 100 ;

  Produto.Prod.cEANTrib  := '7896523206646';
  Produto.Prod.uTrib     := 'UN';
  Produto.Prod.qTrib     := 1;
  Produto.Prod.vUnTrib   := 100;

  Produto.Prod.vOutro    := 0;
  Produto.Prod.vFrete    := 0;
  Produto.Prod.vSeg      := 0;
  Produto.Prod.vDesc     := 0;

  Produto.Prod.CEST := '1111111';

  Produto.infAdProd      := 'Informacao Adicional do Produto';

  Produto.Imposto.vTotTrib := 0;
  Produto.Imposto.ICMS.CST          := cst00;
  Produto.Imposto.ICMS.orig    := oeNacional;
  Produto.Imposto.ICMS.modBC   := dbiValorOperacao;
  Produto.Imposto.ICMS.vBC     := 100;
  Produto.Imposto.ICMS.pICMS   := 18;
  Produto.Imposto.ICMS.vICMS   := 18;
  Produto.Imposto.ICMS.modBCST := dbisMargemValorAgregado;
  Produto.Imposto.ICMS.pMVAST  := 0;
  Produto.Imposto.ICMS.pRedBCST:= 0;
  Produto.Imposto.ICMS.vBCST   := 0;
  Produto.Imposto.ICMS.pICMSST := 0;
  Produto.Imposto.ICMS.vICMSST := 0;
  Produto.Imposto.ICMS.pRedBC  := 0;

  Produto.Imposto.ICMSUFDest.vBCUFDest      := 0.00;
  Produto.Imposto.ICMSUFDest.pFCPUFDest     := 0.00;
  Produto.Imposto.ICMSUFDest.pICMSUFDest    := 0.00;
  Produto.Imposto.ICMSUFDest.pICMSInter     := 0.00;
  Produto.Imposto.ICMSUFDest.pICMSInterPart := 0.00;
  Produto.Imposto.ICMSUFDest.vFCPUFDest     := 0.00;
  Produto.Imposto.ICMSUFDest.vICMSUFDest    := 0.00;
  Produto.Imposto.ICMSUFDest.vICMSUFRemet   := 0.00;

  ANFe.Total.ICMSTot.vBC     := 100;
  ANFe.Total.ICMSTot.vICMS   := 18;
  ANFe.Total.ICMSTot.vBCST   := 0;
  ANFe.Total.ICMSTot.vST     := 0;
  ANFe.Total.ICMSTot.vProd   := 100;
  ANFe.Total.ICMSTot.vFrete  := 0;
  ANFe.Total.ICMSTot.vSeg    := 0;
  ANFe.Total.ICMSTot.vDesc   := 0;
  ANFe.Total.ICMSTot.vII     := 0;
  ANFe.Total.ICMSTot.vIPI    := 0;
  ANFe.Total.ICMSTot.vPIS    := 0;
  ANFe.Total.ICMSTot.vCOFINS := 0;
  ANFe.Total.ICMSTot.vOutro  := 0;
  ANFe.Total.ICMSTot.vNF     := 100;

  ANFe.Total.ICMSTot.vTotTrib := 0;

  ANFe.Total.ICMSTot.vFCPUFDest   := 0.00;
  ANFe.Total.ICMSTot.vICMSUFDest  := 0.00;
  ANFe.Total.ICMSTot.vICMSUFRemet := 0.00;

  ANFe.Transp.modFrete := mfContaEmitente;
  ANFe.Transp.Transporta.CNPJCPF  := '';
  ANFe.Transp.Transporta.xNome    := '';
  ANFe.Transp.Transporta.IE       := '';
  ANFe.Transp.Transporta.xEnder   := '';
  ANFe.Transp.Transporta.xMun     := '';
  ANFe.Transp.Transporta.UF       := '';

  ANFe.Transp.veicTransp.placa := '';
  ANFe.Transp.veicTransp.UF    := '';
  ANFe.Transp.veicTransp.RNTC  := '';

  Volume := ANFe.Transp.Vol.Add;
  Volume.qVol  := 1;
  Volume.esp   := 'Especie';
  Volume.marca := 'Marca';
  Volume.nVol  := 'Numero';
  Volume.pesoL := 100;
  Volume.pesoB := 110;

  ANFe.Cobr.Fat.nFat  := 'Numero da Fatura';
  ANFe.Cobr.Fat.vOrig := 100 ;
  ANFe.Cobr.Fat.vDesc := 0 ;
  ANFe.Cobr.Fat.vLiq  := 100 ;

  Duplicata := ANFe.Cobr.Dup.Add;
  Duplicata.nDup  := '1234';
  Duplicata.dVenc := now+10;
  Duplicata.vDup  := 50;

  Duplicata := ANFe.Cobr.Dup.Add;
  Duplicata.nDup  := '1235';
  Duplicata.dVenc := now+10;
  Duplicata.vDup  := 50;


  ANFe.InfAdic.infCpl     :=  '';
  ANFe.InfAdic.infAdFisco :=  '';

  ObsComplementar := ANFe.InfAdic.obsCont.Add;
  ObsComplementar.xCampo := 'ObsCont';
  ObsComplementar.xTexto := 'Texto';

  ObsFisco := ANFe.InfAdic.obsFisco.Add;
  ObsFisco.xCampo := 'ObsFisco';
  ObsFisco.xTexto := 'Texto';

  ANFe.exporta.UFembarq   := '';;
  ANFe.exporta.xLocEmbarq := '';

  ANFe.compra.xNEmp := '';
  ANFe.compra.xPed  := '';
  ANFe.compra.xCont := '';


end;

procedure pcnGerarXML.GerarXML;
const
sxml = '<NFe xmlns="http://www.portalfiscal.inf.br/nfe"><infNFe versao="3.10" Id="NFe00991200000000000000000000000000001000100011">'+
'<ide><cUF>00</cUF><cNF>00010001</cNF><natOp/><indPag>0</indPag><mod>00</mod><serie>0</serie><nNF>0</nNF><dhEmi>1899-12-30T00:00:00-03:00'+
'</dhEmi><tpNF>0</tpNF><idDest>1</idDest><cMunFG>0000000</cMunFG><tpImp>0</tpImp><tpEmis>1</tpEmis><cDV>1</cDV><tpAmb>1</tpAmb><finNFe>1'+
'</finNFe><indFinal>0</indFinal><indPres>0</indPres><procEmi>0</procEmi><verProc/></ide><emit><CNPJ>00000000000000</CNPJ><xNome>EMPRESA DE'+
' TESTE</xNome><xFant>EMPRESA DE TESTE</xFant><enderEmit><xLgr>RUA DE TESTE</xLgr><nro>S/N</nro><xBairro>ZONA RURAL</xBairro><cMun>1</cMun>'+
'<xMun>NEW YORK</xMun><UF>MA</UF><CEP>99999999</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>000000000001</fone></enderEmit><IE>'+
'0000000000001</IE><IM>2648800</IM><CNAE>6201500</CNAE><CRT>3</CRT></emit><dest><CNPJ>05481336000137</CNPJ><xNome>D.J. COM. E LOCACAO'+
' DE SOFTWARES LTDA - ME</xNome><enderDest><xLgr>Rua Coronel Aureliano de Camargo</xLgr><nro>973</nro><xBairro>Centro</xBairro><cMun>'+
'3554003</cMun><xMun>Tatui</xMun><UF>SP</UF><CEP>18270170</CEP><cPais>1058</cPais><xPais>BRASIL</xPais><fone>1532599600</fone></enderDest>'+
'<indIEDest>1</indIEDest><IE>687138770110</IE></dest><det nItem="1"><prod><cProd>123456</cProd><cEAN>7896523206646</cEAN><xProd>TESTE DE PRODUTO'+
'</xProd><NCM>94051010</NCM><CEST>1111111</CEST><CFOP>5101</CFOP><uCom>UN</uCom><qCom>1.0000</qCom><vUnCom>100.0000000000</vUnCom><vProd>100.00'+
'</vProd><cEANTrib>7896523206646</cEANTrib><uTrib>UN</uTrib><qTrib>1.0000</qTrib><vUnTrib>100.0000000000</vUnTrib><indTot>1</indTot></prod>'+
'<imposto><ICMS><ICMS00><orig>0</orig><CST>00</CST><modBC>3</modBC><vBC>100.00</vBC><pICMS>18.0000</pICMS><vICMS>18.00</vICMS></ICMS00>'+
'</ICMS></imposto><infAdProd>Informacao Adicional do Produto</infAdProd></det><total><ICMSTot><vBC>100.00</vBC><vICMS>18.00</vICMS>'+
'<vICMSDeson>0.00</vICMSDeson><vBCST>0.00</vBCST><vST>0.00</vST><vProd>100.00</vProd><vFrete>0.00</vFrete><vSeg>0.00</vSeg><vDesc>'+
'0.00</vDesc><vII>0.00</vII><vIPI>0.00</vIPI><vPIS>0.00</vPIS><vCOFINS>0.00</vCOFINS><vOutro>0.00</vOutro><vNF>100.00</vNF></ICMSTot>'+
'</total><transp><modFrete>0</modFrete><vol><qVol>1</qVol><esp>Especie</esp><marca>Marca</marca><nVol>Numero</nVol><pesoL>100.000</pesoL>'+
'<pesoB>110.000</pesoB></vol></transp><cobr><fat><nFat>Numero da Fatura</nFat><vOrig>100.00</vOrig><vLiq>100.00</vLiq></fat><dup><nDup>1234'+
'</nDup><dVenc>2018-01-12</dVenc><vDup>50.00</vDup></dup><dup><nDup>1235</nDup><dVenc>2018-01-12</dVenc><vDup>50.00</vDup></dup></cobr>'+
'<infAdic><obsCont xCampo="ObsCont"><xTexto>Texto</xTexto></obsCont><obsFisco xCampo="ObsFisco"><xTexto>Texto</xTexto></obsFisco></infAdic></infNFe></NFe>';
var
 VNFe : TNFe;
 VObj : TNFeW;
begin
  VNFe := TNFe.Create;
  PreecherNFE(VNFe);
  VObj := TNFeW.Create(VNFe);
  VObj.GerarXml;
  try
    CheckEquals(VObj.Gerador.ArquivoFormatoXML, sxml);
  finally
   VObj.Free;
   VNFe.Free;
  end;

end;

initialization
  RegisterTest('ACBrDFe.pcnGerarXML', pcnGerarXML{$ifndef FPC}.suite{$endif});


end.
