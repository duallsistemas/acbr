program TEFAPIDemo;

uses
  Forms,
  configuraserial in 'configuraserial.pas' {frConfiguraSerial},
  frIncluirPagamento in 'frIncluirPagamento.pas' {FormIncluirPagamento},
  frMenuTEF in 'frMenuTEF.pas' {FormMenuTEF},
  frObtemCampo in 'frObtemCampo.pas' {FormObtemCampo},
  frPrincipal in 'frPrincipal.pas' {FormPrincipal},
  uVendaClass in 'uVendaClass.pas',
  frExibeMensagem in 'frExibeMensagem.pas',
  ACBrTEFAPI in '..\..\..\..\Fontes\ACBrTEFD\ACBrTEFAPI.pas',
  ACBrTEFAPICliSiTef in '..\..\..\..\Fontes\ACBrTEFD\ACBrTEFAPICliSiTef.pas',
  ACBrTEFAPIComum in '..\..\..\..\Fontes\ACBrTEFD\ACBrTEFAPIComum.pas',
  ACBrTEFComum in '..\..\..\..\Fontes\ACBrTEFD\ACBrTEFComum.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'TEFAPIDemo';
  Application.CreateForm(TFormPrincipal, FormPrincipal);
  Application.Run;
end.

