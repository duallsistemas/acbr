rmdir /Q /S __history
rmdir /Q /S Win32
rmdir /Q /S .svn
rmdir /Q /S __recovery

del /s *.identcache
del /s *.dproj.local
del /s desktop.ini
del /s *.stat

del /s *.dcu
del /s *.~
del /s *.pas.~*
del /s *.cbk
del /s *.ppu
del /s *.ddp
del /s *.bak
del /s *.o
del /s *.rsm

